package dev.customfmt

import com.intellij.ide.plugins.PluginManager
import com.intellij.openapi.actionSystem.ActionManager
import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.ServiceManager
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage
import org.jdom.Element
import java.util.*

data class CommandConfig(
    val id: String = randomId(),
    val name: String = "",
    val executable: String = "",
    val arguments: String = "",
    val workingDirectory: String = ""
) {
    internal fun xmlElement(): Element {
        return Element("ExecutableConfig").apply {
            setAttribute("id", this@CommandConfig.id)
            setAttribute("name", this@CommandConfig.name)
            setAttribute("executable", this@CommandConfig.executable)
            setAttribute("arguments", this@CommandConfig.arguments)
            setAttribute("workingDirectory", this@CommandConfig.workingDirectory)
        }
    }

    companion object {
        fun randomId() = Settings.actionPrefix + UUID.randomUUID().toString()

        internal fun fromXmlElement(e: Element) =
            CommandConfig(
                e.getAttributeValue("id", ""),
                e.getAttributeValue("name", ""),
                e.getAttributeValue("executable", ""),
                e.getAttributeValue("arguments", ""),
                e.getAttributeValue("workingDirectory", "")
            )

        val Empty = CommandConfig(id = "")
    }
}

@State(name = "CustomFmtConfig", storages = [Storage("dev-customfmt.xml")])
class Settings : PersistentStateComponent<Element> {
    var execs: List<CommandConfig> = listOf()
        set(v) {
            field = v

            updateActions()
        }

    override fun getState(): Element {
        val root = Element("CustomFmtConfig")

        execs.forEach { root.addContent(it.xmlElement()) }

        return root
    }

    override fun loadState(state: Element) {
        execs = state.children.map {
            CommandConfig.fromXmlElement(it)
        }.toList()

        updateActions()
    }

    private fun updateActions() {
        val plugin = PluginManager.getPluginByClassName(javaClass.name)

        ActionManager.getInstance().apply {
            val l = getActionIds(actionPrefix).toSet()
            val r = execs
            val table = (l + r).map { e ->
                Pair<String?, CommandConfig?>(l.firstOrNull { e == it }, r.firstOrNull { e == it })
            }

            table.forEach {
                when {
                    it.first != null && it.second != null -> replaceAction(it.first!!, it.second!!.let {
                        FormatterAction(it.name, it)
                    })
                    it.first == null && it.second != null -> it.second?.let {
                        registerAction(
                            it.id,
                            FormatterAction(it.name, it),
                            plugin
                        )
                    }
                    it.first != null && it.second == null -> it.first?.let { unregisterAction(it) }
                }
            }
        }
    }

    companion object {
        const val actionPrefix = "action-customfmt-"

        val instance: Settings
            get() = ServiceManager.getService(Settings::class.java)
    }
}
