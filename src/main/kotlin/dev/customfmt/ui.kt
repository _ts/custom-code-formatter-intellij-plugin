package dev.customfmt

import com.intellij.openapi.actionSystem.ActionToolbarPosition
import com.intellij.openapi.fileChooser.FileChooserDescriptorFactory
import com.intellij.openapi.options.BoundConfigurable
import com.intellij.openapi.ui.DialogPanel
import com.intellij.ui.AddDeleteListPanel
import com.intellij.ui.JBSplitter
import com.intellij.ui.ToolbarDecorator
import com.intellij.ui.layout.*
import java.awt.Color
import java.awt.Component
import javax.swing.DefaultListCellRenderer
import javax.swing.JList
import javax.swing.ListCellRenderer
import javax.swing.ListSelectionModel
import javax.swing.border.EmptyBorder
import javax.swing.event.ListDataEvent
import javax.swing.event.ListDataListener

private val spacing = createIntelliJSpacingConfiguration()

fun smallBorder() = EmptyBorder(0, 0, 0, spacing.verticalGap)

internal class CommandConfigListCellRenderer : DefaultListCellRenderer() {
    override fun getListCellRendererComponent(
        list: JList<*>?,
        value: Any?,
        index: Int,
        isSelected: Boolean,
        cellHasFocus: Boolean
    ): Component? {
        val component =
            super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus) as DefaultListCellRenderer
        val cmd = value as CommandConfig

        if (cmd.name.isBlank()) {
            component.border = EmptyBorder(0, spacing.verticalGap, 0, 0)
            component.text = "?"
            component.foreground = Color.RED
        } else {
            component.text = cmd.name
        }

        return component
    }
}

class ConfigList(
    initialConfig: List<CommandConfig>,
    val changeAction: (List<CommandConfig>) -> Unit = {},
    val selectionAction: (CommandConfig) -> Unit = {}
) :
    AddDeleteListPanel<CommandConfig>(null, initialConfig) {
    init {
        myList.selectionMode = ListSelectionModel.SINGLE_SELECTION
        myList.addListSelectionListener {
            myList.selectedValue?.let {
                selectionAction(it)
            }
        }
        myListModel.addListDataListener(object : ListDataListener {
            override fun contentsChanged(e: ListDataEvent) {}

            override fun intervalRemoved(e: ListDataEvent) {
                changeAction(configurations)
            }

            override fun intervalAdded(e: ListDataEvent) {
                changeAction(configurations)
            }
        })

        myList.setEmptyText("")
    }

    var configurations: List<CommandConfig>
        set(v) {
            myListModel.removeAllElements()
            myListModel.addAll(v)
        }
        get() = myListModel.elements().toList()

    fun replaceConfigById(config: CommandConfig) =
        myListModel.elements().toList().withIndex().find { it.value.id == config.id }?.index?.let {
            myListModel.setElementAt(config, it)
        }

    override fun findItemToAdd(): CommandConfig {
        return CommandConfig()
    }

    override fun customizeDecorator(decorator: ToolbarDecorator) {
        decorator.setToolbarPosition(ActionToolbarPosition.TOP)
    }

    override fun getListCellRenderer(): ListCellRenderer<*> = CommandConfigListCellRenderer()
}

fun Cell.configList(
    getter: () -> List<CommandConfig>,
    setter: (List<CommandConfig>) -> Unit,
    changeAction: (List<CommandConfig>) -> Unit,
    selectionAction: (CommandConfig) -> Unit
) = configList(PropertyBinding(getter, setter), changeAction, selectionAction)

fun Cell.configList(
    binding: PropertyBinding<List<CommandConfig>>,
    changeAction: (List<CommandConfig>) -> Unit,
    selectionAction: (CommandConfig) -> Unit
) =
    component(ConfigList(binding.get(), changeAction, selectionAction)).withBinding(
        ConfigList::configurations.getter,
        ConfigList::configurations.setter,
        binding
    )

class SettingsDialog : BoundConfigurable("Custom Code Formatter") {

    private var selected = CommandConfig.Empty
    private lateinit var configListComponent: ConfigList

    private lateinit var rightPanelInputs: Row

    override fun createPanel(): DialogPanel {
        val right = panel {
            rightPanelInputs = titledRow("Set executable") {
                row("Name:") { textField({ selected.name }, { selected = selected.copy(name = it) }) }
                row("Executable:") {
                    textFieldWithBrowseButton(
                        { selected.executable },
                        { selected = selected.copy(executable = it) },
                        fileChooserDescriptor = FileChooserDescriptorFactory.createSingleFileDescriptor()
                    )
                }
                row("Arguments:") {
                    textField( // TODO expandableTextField as of 2020.2?
                        { selected.arguments },
                        { selected = selected.copy(arguments = it) }
                    )
                }
                row("Working directory:") {
                    textFieldWithBrowseButton(
                        { selected.workingDirectory },
                        { selected = selected.copy(workingDirectory = it) },
                        fileChooserDescriptor = FileChooserDescriptorFactory.createSingleFolderDescriptor()
                    )
                }
                subRowsEnabled = false
            }.onGlobalApply {
                configListComponent.replaceConfigById(selected)
            }.onGlobalReset {
                rightPanelInputs.subRowsEnabled = selected != CommandConfig.Empty
            }
        }.withBorder(smallBorder())

        val left = panel {
            row {
                val cfg = configList({ Settings.instance.execs }, { }, {
                    if (it.isEmpty()) {
                        selected = CommandConfig.Empty
                        right.reset()
                    }
                }) { config ->
                    right.apply()
                    selected = config
                    println(selected)
                    right.reset() // triggers all getter
                }.constraints(grow, push)
                configListComponent = cfg.component
            }.onGlobalReset {
                selected = CommandConfig.Empty
            }
        }

        val splitter = JBSplitter(true, 0.5f, 0.3f, 0.5f)

        splitter.firstComponent = left
        splitter.secondComponent = right

        right.isEnabled = false

        return panel(LCFlags.fill) {
            row {
                component(splitter)
                    .constraints(grow, push)
                    .onIsModified { left.isModified() || right.isModified() }
                    .onReset { left.reset(); right.reset() }
                    .onApply { left.apply(); right.apply() }

                subRowIndent = 0
            }
        }
    }

    override fun apply() {
        super.apply()

        Settings.instance.execs = configListComponent.configurations
    }
}