package dev.customfmt

import com.intellij.openapi.actionSystem.ActionGroup
import com.intellij.openapi.actionSystem.ActionManager
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.application.runUndoTransparentWriteAction
import com.intellij.openapi.fileEditor.FileEditorManager
import com.intellij.util.containers.map2Array
import com.intellij.util.containers.toArray
import org.jetbrains.annotations.NotNull
import java.io.File
import java.io.IOException

class FormatterGroup : ActionGroup() {
    override fun update(e: AnActionEvent) {
        templatePresentation.isEnabled = e.project?.isOpen ?: false
    }

    override fun getChildren(e: AnActionEvent?): Array<AnAction> {
        return ActionManager.getInstance().let { a ->
            a.getActionIds("action-customfmt-").map2Array {
                a.getAction(it)
            }.ifEmpty { arrayOf() }
        }
    }
}

class FormatterAction(description: String, val commandConfig: CommandConfig) : AnAction(description) {

    override fun update(e: AnActionEvent) {
    }

    override fun actionPerformed(@NotNull e: AnActionEvent) {
        if (commandConfig.executable.isBlank()) {
            return
        }
        e.project?.let { it ->
            FileEditorManager.getInstance(it).selectedTextEditor
                ?.document
                ?.let { doc ->
                    try {
                        val arguments = listOf(commandConfig.arguments, doc.text).filter { it.isNotBlank() }.toTypedArray()
                        val process = ProcessBuilder(commandConfig.executable, *arguments)
                            .also { pb ->
                                if (commandConfig.workingDirectory.isNotBlank()) {
                                    File(commandConfig.workingDirectory).takeIf { it.isDirectory }?.let {
                                        pb.directory(it)
                                    }
                                }

                                println(pb.command())
                            }
                            .start()

                        val newCode = process.inputStream.bufferedReader().readText()
                        process.inputStream.close()

                        if (process.waitFor() == 0) {
                            runUndoTransparentWriteAction { doc.setText(newCode) }
                        }
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
        }
    }
}