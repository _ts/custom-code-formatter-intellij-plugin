# Custom Code Formatter - an IntelliJ Plugin

_Works in Intellij, Clion, PyCharm and other IntelliJ based IDEs._

Originated from the idea to use a custom code formatter, like black,
in PyCharm Notebooks (and not the .ipynb file).

**Features**
* undo changes
* custom key mapping for each command (Settings | Keymap)

Latest release: 0.1 (not yet published)

## How It Works

**Settings:**
Settings | Tools | Custom Code Formatter

**Actions under**:
Code | Custom Code Formatter

TODO
